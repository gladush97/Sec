package com.vanya.dao;

import com.vanya.model.Customer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
@Component
public class CustomerDao {
    private static Map<String,Customer> users=new HashMap<>();

    public static void storeUser(Customer user){
        users.put(user.getName(),user);
    }

    public static void deleteUser(String  login){
        users.remove(login);
    }
    public static Customer getCustomer(String login){
        return users.get(login);
    }


}
