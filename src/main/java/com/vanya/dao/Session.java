package com.vanya.dao;


/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
public class Session {
    private String id;
    private byte[] data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
