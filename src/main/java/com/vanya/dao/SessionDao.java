package com.vanya.dao;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
@Component
public class SessionDao {
    private Map<String ,Session> m=new HashMap<>();
    public void delete(String id) {
        m.remove(id);
    }
    public void save(Session s){
        m.put(s.getId(),s);
    }

    public Optional<Session> getSession(String id) {
        return Optional.ofNullable(m.get(id));
    }
}
