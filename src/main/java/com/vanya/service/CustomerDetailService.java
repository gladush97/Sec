package com.vanya.service;

import com.vanya.dao.CustomerDao;
import com.vanya.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;
import java.util.Arrays;

/**
 * Created by Hladush Ivan
 * on 02.12.16.
 */
@Service
public class CustomerDetailService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Customer user = CustomerDao.getCustomer(login);
        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole());
        UserDetails userDetails = (UserDetails)new User(user.getName(),
                user.getPassword(), Arrays.asList(authority));
        return userDetails;
    }


}
