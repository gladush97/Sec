package com.vanya.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
public class Customer {
    @NotNull
    @Size(min = 3,max=10)
    private String name;

    @NotNull
    @Size(min = 3,max=10)
    private String password;

    private String role;
    public Customer() {
    }

    public Customer(String name, String password, String role) {
        this.name = name;
        this.password = password;
        this.role=role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
