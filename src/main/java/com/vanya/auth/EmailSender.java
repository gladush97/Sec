package com.vanya.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Properties;

/**
 * @author Ivan Gladush
 * @since 29.11.16.
 */
@Component
public class EmailSender {

    private JavaMailSenderImpl mailSender;

    @Value("${mail.port}")
    private String port;

    @Value("${mail.host}")
    private String host;

    @Value("${mail.username}")
    private String userName;

    @Value("${mail.password}")
    private String password;

    @Value("${mail.protocol}")
    private String protocol;

    @Value("${email.template.confirmation.email}")
    private String emailTemplate;

    @PostConstruct
    public void afterPropertySet() {
        mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(Integer.valueOf(port));
        mailSender.setUsername(userName);
        mailSender.setPassword(password);
        mailSender.setProtocol(protocol);
        Properties properties = new Properties();
        properties.setProperty("mail.smtps.auth","true");
        mailSender.setJavaMailProperties(properties);
        send();
    }

    public void send() {
        try {
            MimeMessageHelper simpleMailMessage = new MimeMessageHelper(mailSender.createMimeMessage(),true);
            simpleMailMessage.setFrom("gladush97@gmail.com");
            simpleMailMessage.setSubject("This is first email");
            simpleMailMessage.setTo("gladush97@gmail.com");
            simpleMailMessage.setText(String.format(emailTemplate,"http://localhost/","link","link"),true);
            mailSender.send(simpleMailMessage.getMimeMessage());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
