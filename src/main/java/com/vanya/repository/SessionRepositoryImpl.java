package com.vanya.repository;

import com.vanya.dao.Session;
import com.vanya.dao.SessionDao;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.session.ExpiringSession;
import org.springframework.session.MapSession;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
@Repository
public class SessionRepositoryImpl implements SessionRepository<ExpiringSession> {
    private static final Logger LOG = getLogger(SessionRepositoryImpl.class);

    public SessionRepositoryImpl(){
        System.out.println("ASDDDDDDDDDDDDDDDDDD");
    }
    @Value("${server.session.timeout}")
    private Integer defaultMaxInactiveInterval;

    @Autowired
    private SessionDao sessionDao;

    @Override
    public ExpiringSession createSession() {
        ExpiringSession result = new MapSession();
        if (defaultMaxInactiveInterval != null) {
            result.setMaxInactiveIntervalInSeconds(defaultMaxInactiveInterval);
        }
        return result;
    }

    @Override
    public void save(ExpiringSession expiringSession) {
        Session session = new Session();
        session.setId(expiringSession.getId());
        try {
            session.setData(serialize(expiringSession));
            sessionDao.save(session);
        } catch (IOException e) {
            LOG.error("unable to serialize session", e);
        }

    }

    @Override
    public ExpiringSession getSession(String id) {
        LOG.info("getSession: [{}]", id);
        Optional<Session> savedOptional = sessionDao.getSession(id);

        if (!savedOptional.isPresent()) {
            return null;
        }

        Session saved = savedOptional.get();
        try {
            ExpiringSession mapSession = deserialize(saved.getData());
            if (mapSession.isExpired()) {
                delete(saved.getId());
                return null;
            }
            return mapSession;
        } catch (IOException | ClassNotFoundException e) {
            LOG.error("unable to read session", e);
            return null;
        }

    }

    @Override
    public void delete(String id) {
        sessionDao.delete(id);
    }

    public static byte[] serialize(ExpiringSession expiringSession) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(bout);
        oout.writeObject(expiringSession);
        return bout.toByteArray();
    }

    private static ExpiringSession deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bin = new ByteArrayInputStream(data);
        ObjectInputStream oin = new ObjectInputStream(bin);
        return (MapSession) oin.readObject();
    }

}
