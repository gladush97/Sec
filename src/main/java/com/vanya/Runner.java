package com.vanya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
@SpringBootApplication
//@EnableSpringHttpSession
public class Runner {
    public static void main(String[] args) {
        SpringApplication.run(Runner.class,args);
    }


}
