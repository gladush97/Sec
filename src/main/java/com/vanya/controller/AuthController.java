package com.vanya.controller;

import com.vanya.dao.CustomerDao;
import com.vanya.model.Customer;
import com.vanya.service.CustomerDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Ivan Gladush
 * @since 15.11.16.
 */
@Component
@RestController
public class AuthController {
    private static final String SESSION_ATTRIBUTE_CUSTOMER_ID = "cid";

    @Autowired
    private CustomerDetailService userDetailsService;

    @Autowired
    public HttpSession httpSession;

    @Autowired
    private CustomerDao customerDao;

    @RequestMapping(value = "/login", method = GET)
    public String getLoginPage() {
        return "login page";
    }

    @RequestMapping(value = "/home", method = GET)
    public String getHomePage() {
        return httpSession.getId();
    }

    @RequestMapping(value = "/secret", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getSecretPage() {
        return "secret page";
    }

    @RequestMapping(value = "/autho", method = POST, consumes = "application/json")
    public String authorizedUser(@Valid @RequestBody Customer user) {
        SecurityProperties.User user1 = new SecurityProperties.User();
        user1.setName(user.getName());

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getName());
        getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userDetails.getUsername(),
                userDetails.getPassword(),
                userDetails.getAuthorities()));
        httpSession.setAttribute("cid", 1);
        return httpSession.getId();
    }
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "logout";
    }
    @RequestMapping(value = "/regist", method = POST, consumes = "application/json")
    public String registerUser(@Valid @RequestBody Customer user) {
        customerDao.storeUser(user);
        return "OK";
    }

    private boolean isAnonymousUser(Authentication authentication) {
        return authentication.isAuthenticated();
    }

}
